﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for Circle game object (cild of CircleHolder) behavior,
/// provides possibility to call CircleHolder's functions from
/// Circle animations
/// </summary>
public class SpriteScript : MonoBehaviour
{
	//Link to parent object's (CircleHolder) circle script to send him messages
	private CircleBehavior myCircleBehavior;
	
	/// <summary>
	/// Set link to parent's circle script
	/// </summary>
	void Awake()
	{
		myCircleBehavior = transform.parent.GetComponent<CircleBehavior>();
	}
	
	/// <summary>
	/// Say the CircleHolder that appearing animation is completed.
	/// Get called by the Appearing animation at the end of one
	/// </summary>
	public void Appeared()
	{
		myCircleBehavior.IsAppeared = true;
	}
	
	/// <summary>
	/// Say the CircleHolder that all animations ended and the
	/// game object ought to be disposed.
	/// Get called from Disappearing and Burst animations at the
	/// end of ones
	/// </summary>
	public void Dispose()
	{
		myCircleBehavior.Dispose();
	}
}
