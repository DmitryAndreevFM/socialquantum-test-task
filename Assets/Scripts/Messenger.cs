﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simple class for permanent message displaying in the center of
/// the screen. Attached to one of game objects in the scene from
/// the beginning to provide ability to display messages before
/// bundles are loaded
/// </summary>
public class Messenger : MonoBehaviour
{
	/* 
     * Style of pause and loading messages (configured from Unity editor)
     * Configuration: text aligned in middle center, default font (Arial)
     * (because we need to display messages even before the resources are
     * loaded from bundles), font size 27, font color white
     */
	public GUIStyle guiStyle;
	
	private string message = "";    //Message to display in the center of the screen
	
	/// <summary>
	/// Display the message
	/// </summary>
	public void OnGUI()
	{
		if (!message.Equals(""))    //If there is any message to display then show it
			GUI.Label(new Rect(0, 0, Screen.width, Screen.height), message, guiStyle);
	}
	
	/// <summary>
	/// Set new message to display.
	/// </summary>
	/// <param name="newMessage">New message. Optionial (by default - clear the previous message)</param>
	public void DisplayMessage(string newMessage = "")
	{
		message = newMessage;
	}
}
