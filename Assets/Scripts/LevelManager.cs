﻿using UnityEngine;

/// <summary>
/// Class for holding and providing all gameplay parameters and events
/// </summary>
/// It may derive MonoBehaviour class and be attached to some game
/// object in the scene for convinient parameters changeing through
/// Unity editor if make all the parameters public 
public class LevelManager
{
	//Level parameters are set and stored as private variables
	private float CircleGenerationProbability 	= 0.02f;	//probability to generate a circle in one frame
	private float circleSizeMin 				= 0.2f;		//minimal size of a circle in units
	private float circleSizeMax 				= 1.6f;		//maximal
	private float circleBaseSpeed 				= 1f;		//minimal speed of (the largest) circle (units per second)
	private float circleSpeedDeviation 			= 2f;		//difference between maximal and minimal circle's speed
	private int   circleScorePointsMin 			= 100;		//score poinst for the largest circle
	private int   circleScorePointsMax 			= 300;		//for the smallest one
	private float levelTime 					= 10f;		//time (in seconds) between levelups
	
	private float timeToLevelUp;	//stores time of next levelup
	private float nextCircleSize { get {return Random.Range(circleSizeMin, circleSizeMax);}	}
	
	/// <summary>
	/// Blank constructor (use StartGame for initialization)
	/// </summary>
	public LevelManager() {}
	
	/// <summary>
	/// Initializes LevelManager and generates first circle.
	/// All other managers have to be initialized at that moment
	/// </summary>
	public void StartGame()
	{
		timeToLevelUp = levelTime;
		SceneManager.CircleManager.GenerateCircle(nextCircleSize);
	}
	
	/// <summary>
	/// Setup circle's speed and score points depending on it's size
	/// </summary>
	/// <param name="circle">Circle to setup</param>
	public void SetupCircle(CircleBehavior circle)
	{
		//Additional variable, equals 0 for larges circles and 1 for smallest ones
		float sizeBonus = (circleSizeMax - circle.Size) / (circleSizeMax - circleSizeMin);
		circle.Speed = circleBaseSpeed + circleSpeedDeviation * sizeBonus;
		circle.ScorePoints = circleScorePointsMin + (int)((circleScorePointsMax - circleScorePointsMin) * sizeBonus);
	}
	
	/// <summary>
	/// Checks wether we need to generatee new circle or levelup.
	/// It's not part of MonoBehavior class! Get called from
	/// SceneManager.Update
	/// </summary>
	public void Update()
	{
		if (Random.value < CircleGenerationProbability)
			SceneManager.CircleManager.GenerateCircle(nextCircleSize);
		if (SceneManager.ScoreManager.GameDuration > timeToLevelUp)
			LevelUp();
	}
	
	/// <summary>
	/// Levels up (changes level parameters to increase difficulty)
	/// and swithes circles' texture set to the next one
	/// </summary>
	private void LevelUp()
	{
		timeToLevelUp += levelTime;				//Set time for next levelup
		Debug.Log(Time.time.ToString("0.00") + ": Level Up");
		circleBaseSpeed += 1f;					//Increase circles' speed
		CircleGenerationProbability += 0.01f;	//Bring more circles
		//Texturize all circles with new textures
		SceneManager.TextureManager.ChangeTextureSetToNext();
	}
}
