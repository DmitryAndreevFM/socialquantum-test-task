﻿using UnityEngine;

/// <summary>
/// Script for managing CircleHolder (with Circle sprite as a child)
/// behavior
/// </summary>
public class CircleBehavior : MonoBehaviour
{
	public CircleManager CircleManager;	//Link to the circle manager by which the circle was generated
	public int ScorePoints = 0;			//Points for that circle
	public float Size					//Size of the circle
	{
		get { return size;}
		set
		{
			transform.localScale = new Vector2(value, value);
			//Set bottom position at which to disappear
			destructionBorder = SceneManager.ScreenBounds.yMin + value;
			size = value;
			//Set other parameters (by LevelManager)
			SceneManager.LevelManager.SetupCircle(this);
			//Clear memory from current sprite (texture)
			Destroy(spriteRenderer.sprite);
			//Texturize (by TextureManager)
			SceneManager.TextureManager.SetTextureToCircle(this);
		}
	}
	private float size;			//Variable for Size field
	
	public float Speed			//Circle's speet towards bottom line (always positive) in units per second
	{
		get{ return speed;}
		set
		{
			speed = value;
			if (isAppeared)		//If Appearing animation completed, then set speed, else it'll be set later
				rigidbody2D.velocity = new Vector2(0f, -speed);		
		}
	}
	private float speed;		//Variable for Speed field
	
	public bool IsAppeared		//Whether the circle finished Appearing animation
	{
		get { return isAppeared;} 
		set
		{
			isAppeared = value;
			if (value)			//If circle just appeared then set speed again (to rigidbody)
				Speed = Speed;
		}
	}
	private bool isAppeared;	//Variable for IsAppeared field
	
	private SpriteRenderer spriteRenderer;	//Link to Circle game object's SpriteRenderer for the sake of efficiency
	private Animator animator;				//Link to Animator
	private float destructionBorder;		//Position at which circle have to disappear
	
	/// <summary>
	/// Sets up private links and perform random rotation (for diversity)
	/// </summary>
	public void Awake()
	{
		spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
		animator = gameObject.GetComponentInChildren<Animator>();
		//Perform random rotation to reduce boredom :)
		transform.Rotate(0f, 0f, Random.Range(0f, 360f));
	}
	
	/// <summary>
	/// Checks whether the circle reached bottom line and
	/// deletes it if yes
	/// </summary>
	public void Update()
	{
		if (transform.position.y < destructionBorder)
		{	//If we are near the bottom line
			SceneManager.ScoreManager.CirclesMissed++;	//Report we've been missed
			rigidbody2D.velocity = new Vector2(0f, 0f);	//Stop moving
			animator.SetTrigger("Disappear");			//Start Disappearing animation
			enabled = false;							//Disable updates and OnMouseDown
			//No need to call Dispose at that moment, it'll be called when animation ends
		}
	}
	
	/// <summary>
	/// Checks whether the circle was clicked, if so
	/// then updates score and starts self-destruction
	/// </summary>
	public void OnMouseDown()
	{
		if (SceneManager.Paused)	//If the game is paused that doesn't count
			return;
		Debug.Log(Time.time.ToString("0.00") + ": Circle (Size " + Size.ToString("0.0")
			+ ", Speed " + (-rigidbody2D.velocity.y).ToString("0.0")
			+ ", Points " + ScorePoints + ") was bursted");
		rigidbody2D.velocity = new Vector2(0f, 0f);		//Stop moving
		SceneManager.ScoreManager.Score += ScorePoints;	//Give score points
		animator.SetTrigger("Burst");					//Start Bursting animation
		gameObject.GetComponent<AudioSource>().audio.Play();//Play noizy sound
		enabled = false;								//Disable updates and OnMouseDown
		//No need to call Dispose at that moment, it'll be called when animation ends
	}
	
	/// <summary>
	/// Sets up new sprite with the texture specified
	/// </summary>
	/// <param name="texture">Texture to apply</param>
	public void SetTexture(Texture2D texture)
	{
		Sprite sprite = Sprite.Create(
			texture, 
			new Rect(0, 0, texture.width, texture.height), 
			new Vector2(0.5f, 0.5f),	//Pivot point in the center
			texture.height / 2			//Pixel scale setup to place full texture on the sprite
		);
		spriteRenderer.sprite = sprite;
	}
	
	/// <summary>
	/// Releases all resource used by the CircleBehavior object.
	/// Get called from animations Disappear and Burst
	/// </summary>
	public void Dispose()
	{
		//Say CircleManager that circle is not exists anymore
		CircleManager.ForgetCircle(this);
		//Clear sprite (texture)
		Destroy(spriteRenderer.sprite);
		Destroy(gameObject);
	}
}
